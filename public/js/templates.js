this["JST"] = this["JST"] || {};

this["JST"]["alt_dropdown"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),alts = locals_.alts,username = locals_.username,alt = locals_.alt;if ( alts)
{
buf.push("<li><a class=\"select-alt-dropdown-item\"><div>" + (jade.escape(null == (jade.interp = username) ? "" : jade.interp)) + "<span class=\"no-alt-label\">(none)</span></div></a></li>");
// iterate alts
;(function(){
  var $$obj = alts;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var alt = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'data-alt-name':(alt), "class": [('select-alt-dropdown-item')] }, {"data-alt-name":true})) + "><div>" + (jade.escape(null == (jade.interp = alt) ? "" : jade.interp)) + "</div></a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var alt = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'data-alt-name':(alt), "class": [('select-alt-dropdown-item')] }, {"data-alt-name":true})) + "><div>" + (jade.escape(null == (jade.interp = alt) ? "" : jade.interp)) + "</div></a></li>");
    }

  }
}).call(this);

if ( alts.length < 5)
{
buf.push("<li><a class=\"add-alt-dropdown-item\"><div>+ Add Alt</div></a></li>");
}
}
else
{
if ( alt)
{
buf.push("<div>" + (jade.escape(null == (jade.interp = alt) ? "" : jade.interp)) + "</div>");
}
else
{
buf.push("<div>" + (jade.escape(null == (jade.interp = username) ? "" : jade.interp)) + "<span class=\"no-alt-label\">(none)</span></div>");
}
};return buf.join("");
};

this["JST"]["battle"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),yourIndex = locals_.yourIndex,yourTeam = locals_.yourTeam,opponentTeam = locals_.opponentTeam,window = locals_.window;for (var i = 0; i < 2; i++)
{
var pokemonClass = (i === yourIndex ? " bottom" : " top")
var team = (i === yourIndex ? yourTeam : opponentTeam).get('pokemon')
for (var j = 0, len = team.length; j < len; j++)
{
buf.push("<div" + (jade.attrs({ 'data-team':(i), 'data-slot':(j), "class": [('pokemon'),('hidden'),(pokemonClass)] }, {"class":true,"data-team":true,"data-slot":true})) + ">");
if ( (team.at(j).attributes.species == 'Zoroark' || team.at(j).attributes.species == 'Zorua') && team.at(j).attributes.percent == 100)
{
buf.push(null == (jade.interp = window.JST['battle_pokemon']({pokemon: team.at(team.length - 1)})) ? "" : jade.interp);
}
else
{
buf.push(null == (jade.interp = window.JST['battle_pokemon']({pokemon: team.at(j)})) ? "" : jade.interp);
}
buf.push("</div>");
}
};return buf.join("");
};

this["JST"]["battle_actions"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),window = locals_.window,validActions = locals_.validActions,yourTeam = locals_.yourTeam;var moveButton_mixin = function(forPokemon, i){
var block = this.block, attributes = this.attributes || {}, escaped = this.escaped || {};
var moveName   = forPokemon.get('moves')[i]
var origName   = moveName
var moveType   = forPokemon.get('moveTypes')[i].toLowerCase()
var pp         = forPokemon.get('pp')[i]
var maxPP      = forPokemon.get('maxPP')[i]
var isEnabled  = validMoves.indexOf(moveName) !== -1
var disabledClass = (!isEnabled ? ' disabled' : '')
var bigClass   = (validMoves.length === 1 ? ' big' : '')
var spanClass  = (validMoves.length === 1 ? 'span12' : 'span6')
if ( moveName === 'Hidden Power')
{
moveType = window.HiddenPower.BW.type(forPokemon.get('ivs'))
moveName += ' (' + moveType + ')'
moveType = moveType.toLowerCase()
}
var moveClass  = moveType + disabledClass + bigClass
buf.push("<div" + (jade.attrs({ "class": [(spanClass)] }, {"class":true})) + "><div" + (jade.attrs({ 'data-move-id':(origName), "class": [('move'),('button'),(moveClass)] }, {"class":true,"data-move-id":true})) + "><div class=\"main_text\">" + (jade.escape(null == (jade.interp = moveName) ? "" : jade.interp)) + "</div><div class=\"meta_info\">" + (jade.escape(null == (jade.interp = pp) ? "" : jade.interp)) + "<small>/" + (jade.escape((jade.interp = maxPP) == null ? '' : jade.interp)) + "</small></div></div></div>");
};
var validMoves = validActions.moves || []
var validSwitches = validActions.switches || []
buf.push("<div class=\"moves span8\"><h2>Moves</h2>");
var pokemon = yourTeam.at(0)
var numMoves = pokemon.get('moves').length
if ( validMoves.length === 1)
{
var moveIndex = pokemon.get('moves').indexOf(validMoves[0])
buf.push("<div class=\"row-fluid\">");
if ( moveIndex === -1)
{
buf.push("<div class=\"span12\"><div" + (jade.attrs({ 'data-move-id':(validMoves[0]), "class": [('move'),('button'),('normal'),('big')] }, {"data-move-id":true})) + "><div class=\"main_text\">" + (jade.escape(null == (jade.interp = validMoves[0]) ? "" : jade.interp)) + "</div></div></div>");
}
else
{
moveButton_mixin(pokemon, moveIndex);
}
buf.push("</div>");
}
else
{
for (var i = 0; i < numMoves; i += 2)
{
buf.push("<div class=\"row-fluid\">");
moveButton_mixin(pokemon, i);
if ( i + 1 < numMoves)
{
moveButton_mixin(pokemon, i + 1);
}
buf.push("</div>");
}
}
buf.push("<div class=\"mega-evolve button hidden\">Mega Evolve</div></div><div class=\"switches span4\"><h2>Pokemon</h2>");
var teamPokemon = yourTeam.get('pokemon')
for (var j = 0, len = teamPokemon.length; j < len; j += 3)
{
buf.push("<div class=\"row-fluid\">");
for (var i = j; i < j + 3; i++)
{
var pokemon = teamPokemon.at(i)
var isEnabled = validSwitches.indexOf(i) !== -1
var disabledClass = (!isEnabled ? 'disabled' : '')
if ( pokemon)
{
buf.push("<div class=\"span4\"><div" + (jade.attrs({ 'data-slot':(i), "class": [('switch'),('button'),(disabledClass)] }, {"class":true,"data-slot":true})) + "><div" + (jade.attrs({ 'style':(window.PokemonIconBackground(pokemon)), "class": [('pokemon_icon')] }, {"style":true})) + "></div></div></div>");
}
}
buf.push("</div>");
}
buf.push("</div><div class=\"show_spinner flex-center fill bg-faded-white hidden\">" + (null == (jade.interp = window.JST['spinner']()) ? "" : jade.interp) + "</div>");;return buf.join("");
};

this["JST"]["battle_controls"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),speeds = locals_.speeds,currentSpeed = locals_.currentSpeed;buf.push("<div class=\"row-fluid\"><div class=\"span4\"><h2>Battle speed</h2>");
// iterate speeds
;(function(){
  var $$obj = speeds;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var speed = $$obj[name];

buf.push("<label class=\"block\"><input" + (jade.attrs({ 'type':("radio"), 'name':("battle_speed"), 'checked':((speed === currentSpeed)), 'value':(speed), "class": [('battle-speed')] }, {"type":true,"name":true,"checked":true,"value":true})) + "/> " + (jade.escape((jade.interp = name) == null ? '' : jade.interp)) + "</label>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var speed = $$obj[name];

buf.push("<label class=\"block\"><input" + (jade.attrs({ 'type':("radio"), 'name':("battle_speed"), 'checked':((speed === currentSpeed)), 'value':(speed), "class": [('battle-speed')] }, {"type":true,"name":true,"checked":true,"value":true})) + "/> " + (jade.escape((jade.interp = name) == null ? '' : jade.interp)) + "</label>");
    }

  }
}).call(this);

buf.push("</div></div>");;return buf.join("");
};

this["JST"]["battle_end"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),window = locals_.window;buf.push("<div class=\"button big save-replay block relative\">Save replay<div class=\"show_spinner flex-center fill bg-faded-white hidden\">" + (null == (jade.interp = window.JST['spinner']()) ? "" : jade.interp) + "</div></div><div class=\"row-fluid\"><div class=\"button save-log block span6 center\">Save log</div><div class=\"button return-to-lobby block span6 center\">Return to lobby</div></div>");;return buf.join("");
};

this["JST"]["battle_hover_info"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),pokemon = locals_.pokemon;buf.push("<p><strong>HP:</strong> " + (jade.escape((jade.interp = pokemon.getPercentHP()) == null ? '' : jade.interp)) + "%");
if ( pokemon.has('hp'))
{
buf.push(" (" + (jade.escape((jade.interp = pokemon.get('hp')) == null ? '' : jade.interp)) + "/" + (jade.escape((jade.interp = pokemon.get('maxHP')) == null ? '' : jade.interp)) + ")");
}
buf.push("<br/>");
if ( pokemon.has('ability'))
{
buf.push("<strong>Ability:</strong> " + (jade.escape((jade.interp = pokemon.get('ability')) == null ? '' : jade.interp)) + "");
}
else
{
buf.push("<strong>Possible abilities:</strong> " + (jade.escape((jade.interp = pokemon.getAbilities().join(', ')) == null ? '' : jade.interp)) + "");
}
buf.push("<br/><strong>Status:</strong> " + (jade.escape((jade.interp = pokemon.getStatus()) == null ? '' : jade.interp)) + "</p>");
if ( pokemon.has('moves'))
{
buf.push("<div class=\"hover_info_moves\">");
// iterate pokemon.get('moves')
;(function(){
  var $$obj = pokemon.get('moves');
  if ('number' == typeof $$obj.length) {

    for (var i = 0, $$l = $$obj.length; i < $$l; i++) {
      var moveName = $$obj[i];

var moveClass = pokemon.get('moveTypes')[i].toLowerCase()
var pp        = pokemon.get('pp')[i]
var maxPP     = pokemon.get('maxPP')[i]
buf.push("<div" + (jade.attrs({ "class": [('button'),('hover_info'),(moveClass)] }, {"class":true})) + "><div class=\"main_text\">" + (jade.escape(null == (jade.interp = moveName) ? "" : jade.interp)) + "</div><div class=\"meta_info\">" + (jade.escape(null == (jade.interp = pp) ? "" : jade.interp)) + "<small>/" + (jade.escape((jade.interp = maxPP) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  } else {
    var $$l = 0;
    for (var i in $$obj) {
      $$l++;      var moveName = $$obj[i];

var moveClass = pokemon.get('moveTypes')[i].toLowerCase()
var pp        = pokemon.get('pp')[i]
var maxPP     = pokemon.get('maxPP')[i]
buf.push("<div" + (jade.attrs({ "class": [('button'),('hover_info'),(moveClass)] }, {"class":true})) + "><div class=\"main_text\">" + (jade.escape(null == (jade.interp = moveName) ? "" : jade.interp)) + "</div><div class=\"meta_info\">" + (jade.escape(null == (jade.interp = pp) ? "" : jade.interp)) + "<small>/" + (jade.escape((jade.interp = maxPP) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  }
}).call(this);

buf.push("</div>");
};return buf.join("");
};

this["JST"]["battle_list"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),battles = locals_.battles;buf.push("<div class=\"battle-list\"><h2>Battle List (" + (jade.escape((jade.interp = battles.length) == null ? '' : jade.interp)) + " battles)</h2><div class=\"battle-list-collection\">");
if ( battles.length == 0)
{
buf.push("<p class=\"empty\">There are no active battles happening at this time.</p>");
}
// iterate battles
;(function(){
  var $$obj = battles;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var battle = $$obj[$index];

buf.push("<div class=\"battle-list-item-wrapper\"><div" + (jade.attrs({ 'data-battle-id':(battle[0]), "class": [('battle-list-item'),('spectate'),('clearfix')] }, {"data-battle-id":true})) + "><div class=\"left\"><span class=\"player\">" + (jade.escape(null == (jade.interp = battle[1]) ? "" : jade.interp)) + "</span><span class=\"vs\">VS</span><span class=\"player\">" + (jade.escape(null == (jade.interp = battle[2]) ? "" : jade.interp)) + "</span></div><div class=\"right\"><span" + (jade.attrs({ 'data-time-start':(Date.now() - battle[3]), "class": [('fake_link'),('elapsed-time')] }, {"data-time-start":true})) + "></span></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var battle = $$obj[$index];

buf.push("<div class=\"battle-list-item-wrapper\"><div" + (jade.attrs({ 'data-battle-id':(battle[0]), "class": [('battle-list-item'),('spectate'),('clearfix')] }, {"data-battle-id":true})) + "><div class=\"left\"><span class=\"player\">" + (jade.escape(null == (jade.interp = battle[1]) ? "" : jade.interp)) + "</span><span class=\"vs\">VS</span><span class=\"player\">" + (jade.escape(null == (jade.interp = battle[2]) ? "" : jade.interp)) + "</span></div><div class=\"right\"><span" + (jade.attrs({ 'data-time-start':(Date.now() - battle[3]), "class": [('fake_link'),('elapsed-time')] }, {"data-time-start":true})) + "></span></div></div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");;return buf.join("");
};

this["JST"]["battle_pokemon"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),pokemon = locals_.pokemon;var percent = pokemon.getPercentHP()
var color = ((percent <= 25) ? "#f00" : (percent <= 50) ? "#ff0" : "#0f0")
var style = "width: " + percent + "%"
var greenStyle = style + "; background-color: " + color
buf.push("<a" + (jade.attrs({ 'href':(pokemon.getPokedexUrl()), 'target':('_blank'), 'data-species':(pokemon.get('species')), 'data-forme':(pokemon.get('forme')), 'data-shiny':((pokemon.get('shiny') ? "true" : "false")), "class": [('sprite'),('preload')] }, {"href":true,"target":true,"data-species":true,"data-forme":true,"data-shiny":true})) + "></a><div class=\"pokemon-info\"><span class=\"pokemon-meta\"><span class=\"pokemon-name\">" + (jade.escape(null == (jade.interp = pokemon.get('name')) ? "" : jade.interp)) + "</span>");
if ( pokemon.get('gender') === 'F')
{
buf.push("<span class=\"gender gender_female\"> &#9792;</span>");
}
else if ( pokemon.get('gender') === 'M')
{
buf.push("<span class=\"gender gender_male\"> &#9794;</span>");
}
buf.push("<span class=\"pokemon-level\"><span class=\"pokemon-level-text\"> Lv.</span>" + (jade.escape(null == (jade.interp = pokemon.get('level')) ? "" : jade.interp)) + "</span></span><div class=\"hp-text\">" + (jade.escape((jade.interp = percent) == null ? '' : jade.interp)) + "%</div><div" + (jade.attrs({ 'style':(greenStyle), "class": [('hp')] }, {"style":true})) + "></div><div class=\"hp-gradient\"></div><div class=\"pokemon-effects\"></div></div>");;return buf.join("");
};

this["JST"]["battle_team_preview"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),battle = locals_.battle,window = locals_.window;var displayOtherTeam_mixin = function(team, text){
var block = this.block, attributes = this.attributes || {}, escaped = this.escaped || {};
var teamvisible = team.collection.parents[0].get('visibleteam')
var isdef = typeof teamvisible
if ( isdef != 'undefined' && teamvisible)
{
buf.push("<section class=\"battle_team_preview\"><p>" + (jade.escape(null == (jade.interp = text) ? "" : jade.interp)) + "</p><ul class=\"row-fluid unstyled\">");
team.get('pokemon').each(function(pokemon) {
{
buf.push("<li class=\"px_40\">");
displayPokemon_mixin(pokemon);
buf.push("</li>");
}
})
buf.push("</ul></section>");
}
};
var displayYourTeam_mixin = function(team, text){
var block = this.block, attributes = this.attributes || {}, escaped = this.escaped || {};
buf.push("<section class=\"battle_team_preview\"><p>" + (jade.escape(null == (jade.interp = text) ? "" : jade.interp)) + "</p><ul class=\"row-fluid unstyled\">");
for (var i = 0; i < battle.numActive; i++)
{
buf.push("<li class=\"px_40 lead_text\">");
if ( battle.numActive > 1)
{
buf.push("Lead " + (jade.escape((jade.interp = i + 1) == null ? '' : jade.interp)) + "");
}
else
{
buf.push("Lead");
}
buf.push("</li>");
}
buf.push("</ul><ul class=\"arrange_team row-fluid unstyled\">");
yourTeam.get('pokemon').each(function(pokemon, i) {
{
buf.push("<li class=\"px_40 img-polaroid arrange_pokemon\">");
displayPokemon_mixin(pokemon, i);
buf.push("</li>");
}
})
buf.push("</ul><div class=\"button submit_arrangement\">Start battle</div></section>");
};
var displayPokemon_mixin = function(pokemon, i){
var block = this.block, attributes = this.attributes || {}, escaped = this.escaped || {};
buf.push("<div class=\"team_pokemon\">");
var style = window.PokemonIconBackground(pokemon)
buf.push("<div" + (jade.attrs({ 'data-index':(i), 'style':(style), "class": [('pokemon_icon')] }, {"data-index":true,"style":true})) + "></div></div>");
if ( pokemon.get('gender') === 'F')
{
buf.push("<div class=\"gender gender_female\">&#9792;</div>");
}
else if ( pokemon.get('gender') === 'M')
{
buf.push("<div class=\"gender gender_male\">&#9794;</div>");
}
if ( pokemon.get('level') != 120)
{
buf.push("<div class=\"level\">Lv." + (jade.escape((jade.interp = pokemon.get('level')) == null ? '' : jade.interp)) + "</div>");
}
};
buf.push("<div class=\"battle_teams\">");
var theirTeam = battle.getOpponentTeam()
var yourTeam = battle.getTeam()
displayOtherTeam_mixin(theirTeam, theirTeam.get('owner') + "'s team:");
if ( !battle.get('spectating'))
{
displayYourTeam_mixin(yourTeam, "Click Pokemon to switch them around:");
}
else
{
displayOtherTeam_mixin(yourTeam, yourTeam.get('owner') + "'s team:");
}
buf.push("</div>");;return buf.join("");
};

this["JST"]["battle_user_info"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),yourTeam = locals_.yourTeam,opponentTeam = locals_.opponentTeam,yourIndex = locals_.yourIndex,window = locals_.window;// iterate [ yourTeam, opponentTeam ]
;(function(){
  var $$obj = [ yourTeam, opponentTeam ];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var team = $$obj[$index];

var leftOrRight = (team === yourTeam ? 'left' : 'right')
var teamIndex = (team === yourTeam ? yourIndex : 1 - yourIndex)
buf.push("<div" + (jade.attrs({ "class": [('bg-faded-blue'),('fill-' + leftOrRight)] }, {"class":true})) + "><div" + (jade.attrs({ "class": [(leftOrRight)] }, {"class":true})) + "><div class=\"owner_name\">" + (jade.escape(null == (jade.interp = team.get('owner')) ? "" : jade.interp)) + "</div><div class=\"battle-timer frozen-timer hidden\"></div><div class=\"battle-timer remaining-timer\"></div><div class=\"pokemon_icons\">");
var teamvisible = team.collection.parents[0].get('visibleteam')
var isdef = typeof teamvisible
var orig = team.get('pokemon')
var teamPokemon = team.getRandomOrder()
for (var i = 0, len = teamPokemon.length; i < len; i += 1)
{
var pokemon = teamPokemon[i]
if ( pokemon)
{
buf.push("<div class=\"icon_wrapper\">");
var origpkmn = orig.findWhere({species: pokemon.species});
var faintClass = (origpkmn.isFainted() ? 'fainted' : '')
var style = window.PokemonIconBackground(origpkmn)
if ( isdef != 'undefined' &&  teamvisible)
{
buf.push("<a" + (jade.attrs({ 'href':(window.PokemonIconBackground(origpkmn)), 'target':("_blank"), 'style':(style), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
else
{
var hasbeeninbattle = origpkmn.get('beeninbattle')
if ( hasbeeninbattle == true)
{
buf.push("<a" + (jade.attrs({ 'href':(window.PokemonIconBackground(origpkmn)), 'target':("_blank"), 'style':(style), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
else
{
var newstyle = "background:url('../Sprites/Icons/iconempty.png')!important; background-size: cover!important;"
buf.push("<a" + (jade.attrs({ 'href':(""), 'target':("_blank"), 'style':(newstyle), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
}
buf.push("</div>");
}
}
buf.push("</div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var team = $$obj[$index];

var leftOrRight = (team === yourTeam ? 'left' : 'right')
var teamIndex = (team === yourTeam ? yourIndex : 1 - yourIndex)
buf.push("<div" + (jade.attrs({ "class": [('bg-faded-blue'),('fill-' + leftOrRight)] }, {"class":true})) + "><div" + (jade.attrs({ "class": [(leftOrRight)] }, {"class":true})) + "><div class=\"owner_name\">" + (jade.escape(null == (jade.interp = team.get('owner')) ? "" : jade.interp)) + "</div><div class=\"battle-timer frozen-timer hidden\"></div><div class=\"battle-timer remaining-timer\"></div><div class=\"pokemon_icons\">");
var teamvisible = team.collection.parents[0].get('visibleteam')
var isdef = typeof teamvisible
var orig = team.get('pokemon')
var teamPokemon = team.getRandomOrder()
for (var i = 0, len = teamPokemon.length; i < len; i += 1)
{
var pokemon = teamPokemon[i]
if ( pokemon)
{
buf.push("<div class=\"icon_wrapper\">");
var origpkmn = orig.findWhere({species: pokemon.species});
var faintClass = (origpkmn.isFainted() ? 'fainted' : '')
var style = window.PokemonIconBackground(origpkmn)
if ( isdef != 'undefined' &&  teamvisible)
{
buf.push("<a" + (jade.attrs({ 'href':(window.PokemonIconBackground(origpkmn)), 'target':("_blank"), 'style':(style), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
else
{
var hasbeeninbattle = origpkmn.get('beeninbattle')
if ( hasbeeninbattle == true)
{
buf.push("<a" + (jade.attrs({ 'href':(window.PokemonIconBackground(origpkmn)), 'target':("_blank"), 'style':(style), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
else
{
var newstyle = "background:url('../Sprites/Icons/iconempty.png')!important; background-size: cover!important;"
buf.push("<a" + (jade.attrs({ 'href':(""), 'target':("_blank"), 'style':(newstyle), 'data-team':(teamIndex), 'data-slot':(i), "class": [('pokemon_icon'),(faintClass)] }, {"class":true,"href":true,"target":true,"style":true,"data-team":true,"data-slot":true})) + "></a>");
if ( !origpkmn.isFainted())
{
buf.push("<div class=\"pokemon_hp_background\"><div" + (jade.attrs({ 'style':("height: " + (origpkmn.getSpecies().id) + "%"), "class": [('pokemon_hp'),(origpkmn.getHPColor())] }, {"class":true,"style":true})) + "></div></div>");
}
}
}
buf.push("</div>");
}
}
buf.push("</div></div></div>");
    }

  }
}).call(this);
;return buf.join("");
};

this["JST"]["battle_window"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),battle = locals_.battle,window = locals_.window;buf.push("<div" + (jade.attrs({ 'data-battle-id':(battle.id), "class": [('window'),('battle_window')] }, {"data-battle-id":true})) + "><div class=\"battle\">");
var numBackgrounds = 6
var backgroundNumber = parseInt(battle.id.substr(0, 6), 16)
backgroundNumber = (backgroundNumber % numBackgrounds)
buf.push("<div" + (jade.attrs({ "class": [('battle_container'),('battle_bg_' + backgroundNumber)] }, {"class":true})) + "><div class=\"battle_user_info\"></div><div class=\"battle_pane clearfix\"></div><div class=\"battle_summary\"></div><div class=\"battle_overlays\"></div></div><div class=\"battle_actions row-fluid relative\"></div><div class=\"battle-controls\"></div></div><div class=\"chat without_spectators without_chat_input\">" + (null == (jade.interp = window.JST['chat']()) ? "" : jade.interp) + "</div></div>");;return buf.join("");
};

this["JST"]["challenge"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div class=\"challenge_data\"></div><div class=\"challenge_buttons\"><div class=\"is_challenger hidden\"><div class=\"accept_challenge button button_blue\">Accept</div><div class=\"reject_challenge button button_red\">Reject</div></div><div class=\"is_not_challenger\"><div class=\"send_challenge button button_blue\"><span class=\"icon icon-spinner spinner-anim hidden\"></span> <span class=\"challenge_text\">Challenge</span></div><div class=\"cancel_challenge button\">Close</div></div></div>");;return buf.join("");
};

this["JST"]["chat"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div class=\"user_list\"><p><strong class=\"user_count\"></strong></p><ul class=\"users\"></ul></div><div class=\"message_pane\"><div class=\"messages\"></div><div class=\"chat_input_pane\"><input type=\"button\" value=\"Send\" class=\"chat_input_send\"/><div class=\"chat_input_wrapper\"><input type=\"text\" class=\"chat_input\"/></div></div></div>");;return buf.join("");
};

this["JST"]["modals/achievements"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),achievements = locals_.achievements,window = locals_.window;buf.push("<div id=\"achievements-modal\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>Achievements Earned</h3></div><div class=\"modal-body\">");
// iterate achievements
;(function(){
  var $$obj = achievements;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var achievement = $$obj[$index];

buf.push("<div class=\"achievement clearfix\"><img" + (jade.attrs({ 'src':(window.AchievementSprite(achievement)) }, {"src":true})) + "/><div class=\"achievement-info\"><h2>" + (jade.escape(null == (jade.interp = achievement.name) ? "" : jade.interp)) + "</h2><p>" + (jade.escape(null == (jade.interp = achievement.condition) ? "" : jade.interp)) + "</p></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var achievement = $$obj[$index];

buf.push("<div class=\"achievement clearfix\"><img" + (jade.attrs({ 'src':(window.AchievementSprite(achievement)) }, {"src":true})) + "/><div class=\"achievement-info\"><h2>" + (jade.escape(null == (jade.interp = achievement.name) ? "" : jade.interp)) + "</h2><p>" + (jade.escape(null == (jade.interp = achievement.condition) ? "" : jade.interp)) + "</p></div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");;return buf.join("");
};

this["JST"]["modals/credits"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div id=\"credits-modal\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>PokeBattle Credits</h3></div><div class=\"modal-body\"><ul><li>Pokémon is © 1995-2016 Nintendo. All Pokemon names and sprites are\n owned by them.</li><li>Huge thanks to PokeBattle.com for developing this awesome engine\n You can find their engine at <a href=\"http://github.com/sarenji/pokebattle-sim\">this GitHub repo!</a></li><li>Big thanks to Suzey and the rest of the devteam of Insurgence for\n creating Insurgence</li><li>Major thanks to my faithful testers who helped me make sure this thing\n actually worked</li><li>And of course thanks to you, the user, for actually using this thing,\n we love you</li></ul></div><div class=\"modal-footer\"><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button\">Wow, these people rule!</button></div></div>");;return buf.join("");
};

this["JST"]["modals/errors"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),title = locals_.title,body = locals_.body;buf.push("<div id=\"errors-modal\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>" + (jade.escape(null == (jade.interp = title) ? "" : jade.interp)) + "</h3></div><div class=\"modal-body\">" + (null == (jade.interp = body) ? "" : jade.interp) + "</div><div class=\"modal-footer\"><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button\">Back</button></div></div>");;return buf.join("");
};

this["JST"]["modals/export_team"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div id=\"export-team-modal\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>Your exported team</h3></div><div class=\"modal-body\"><form class=\"form-horizontal\"><div class=\"control-group\"><textarea class=\"exported-team textarea_modal\"></textarea></div></form></div><div class=\"modal-footer\"><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button\">Close</button></div></div>");;return buf.join("");
};

this["JST"]["modals/import_team"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div id=\"import-team-modal\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>Import a team from PS/PO</h3></div><div class=\"modal-body\"><div class=\"form-errors alert alert-error hidden\"></div><form class=\"form-horizontal\"><div class=\"control-group\"><textarea placeholder=\"Paste your team here\" class=\"imported-team textarea_modal\"></textarea></div></form></div><div class=\"modal-footer\"><button class=\"button button_blue import-team-submit\">Import</button><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button\">Close</button></div></div>");;return buf.join("");
};

this["JST"]["modals/new_client"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div id=\"new_client\" class=\"modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>A new client appeared!</h3></div><div class=\"modal-body\"><p><strong>Your client is outdated!</strong> Please refresh to upgrade to the latest version of the client. If you\n choose not to refresh, you may see odd behavior. If you were editing\n a team, you should save the team first, then manually refresh.</p></div><div class=\"modal-footer\"><button class=\"button button_blue button_refresh\">Refresh</button><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button\">I'll refresh later</button></div></div>");;return buf.join("");
};

this["JST"]["modals/teams_differ"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),teamText = locals_.teamText;buf.push("<div class=\"teams-differ-modal modal hide\"><div class=\"modal-header\"><button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\">×</button><h3>Our teams differ!</h3></div><div class=\"modal-body\"><p><strong>Your copy of this team differs from ours!</strong></p><p>Perhaps you edited this team on a different computer? Or disconnected\n during a save? Either way, this is the team we have:</p><textarea class=\"remote-team textarea_modal\">" + (jade.escape(null == (jade.interp = teamText) ? "" : jade.interp)) + "</textarea></div><div class=\"modal-footer\"><button data-dismiss=\"modal\" aria-hidden=\"true\" class=\"button button_blue\">Keep local changes</button><button class=\"button button_override\">Use remote version</button></div></div>");;return buf.join("");
};

this["JST"]["modify"] = function anonymous(locals
/**/) {
var buf = [];
;return buf.join("");
};

this["JST"]["move_hover_info"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),move = locals_.move;buf.push("<p><strong>Base Power:</strong>");
if ( move.power == 0)
{
buf.push(" &mdash;");
}
else
{
buf.push(" " + (jade.escape((jade.interp = move.power) == null ? '' : jade.interp)) + "");
}
buf.push("</p><p><strong>Accuracy:</strong>");
if ( move.accuracy == 0)
{
buf.push(" &mdash;");
}
else
{
buf.push(" " + (jade.escape((jade.interp = move.accuracy) == null ? '' : jade.interp)) + "%");
}
buf.push("</p>");
if ( move.priority != 0)
{
buf.push("<p><strong>Priority:</strong>");
if ( move.priority > 0)
{
buf.push(" +" + (jade.escape((jade.interp = move.priority) == null ? '' : jade.interp)) + "");
}
else
{
buf.push(" " + (jade.escape((jade.interp = move.priority) == null ? '' : jade.interp)) + "");
}
buf.push("</p>");
}
buf.push("<p><strong>Description:</strong> " + (jade.escape((jade.interp = move.description) == null ? '' : jade.interp)) + "</p>");;return buf.join("");
};

this["JST"]["navigation"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<p class=\"logo\"></p><h2>Chat</h2><ul class=\"nav nav_rooms\"><li class=\"nav_item fake_link\">Lobby</li></ul><h2>Other links</h2><ul class=\"nav\"><li class=\"nav_item fake_link nav_teambuilder\">Teambuilder</li><li class=\"nav_item fake_link nav_battle_list\">Battle List</li><a href=\"/replays\" target=\"_blank\" class=\"nav_item\">Your Replays</a><a href=\"/leaderboard/\" target=\"_blank\" class=\"nav_item\">Leaderboard</a><a href=\"/tiers/\" target=\"_blank\" class=\"nav_item\">Tier List</a><a href=\"//bitbucket.org/PInsurgence/battle-simulator/issues?status=new&amp;status=open\" target=\"_blank\" class=\"nav_item\">Bug Reports</a><a href=\"//forums.p-insurgence.com/c/battle-simulator/feature-requests\" target=\"_blank\" class=\"nav_item\">Feature Requests</a></ul><ul class=\"nav nav_battles hidden\"></ul><h2 class=\"header_messages hidden\">Messages</h2><ul class=\"nav nav_messages hidden\"></ul>");;return buf.join("");
};

this["JST"]["new_battle"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),window = locals_.window,defaultClauses = locals_.defaultClauses;buf.push("<p><strong>Format:</strong></p><div class=\"dropdown\"><div data-toggle=\"dropdown\" class=\"select select-format\"></div><ul role=\"menu\" class=\"dropdown-menu format-dropdown\">");
var allformats = window.PokeBattle.conditions.Formats()
// iterate allformats
;(function(){
  var $$obj = allformats;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'data-format':("" + (format.name) + ""), "class": [('select-format-dropdown-item')] }, {"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'data-format':("" + (format.name) + ""), "class": [('select-format-dropdown-item')] }, {"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  }
}).call(this);

buf.push("</ul></div><p><strong>Select a team:</strong></p><div class=\"dropdown\"><div data-toggle=\"dropdown\" class=\"select select-team rounded\"><strong>Your team</strong></div><ul role=\"menu\" class=\"dropdown-menu team-dropdown\"></ul></div>");
if ( defaultClauses)
{
buf.push("<p><strong>Clauses:</strong></p><ul class=\"challenge_clauses well\">");
// iterate window.SelectableConditions
;(function(){
  var $$obj = window.SelectableConditions;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var value = $$obj[$index];

var checked = (defaultClauses.indexOf(value) >= 0)
var clause = window._.invert(window.Conditions)[value]
buf.push("<li><label><input" + (jade.attrs({ 'type':("checkbox"), 'value':(value), 'checked':(checked), 'data-clause':(value) }, {"type":true,"value":true,"checked":true,"data-clause":true})) + "/> " + (jade.escape((jade.interp = window.HumanizedConditions['en'][clause]) == null ? '' : jade.interp)) + "</label></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var value = $$obj[$index];

var checked = (defaultClauses.indexOf(value) >= 0)
var clause = window._.invert(window.Conditions)[value]
buf.push("<li><label><input" + (jade.attrs({ 'type':("checkbox"), 'value':(value), 'checked':(checked), 'data-clause':(value) }, {"type":true,"value":true,"checked":true,"data-clause":true})) + "/> " + (jade.escape((jade.interp = window.HumanizedConditions['en'][clause]) == null ? '' : jade.interp)) + "</label></li>");
    }

  }
}).call(this);

buf.push("</ul>");
};return buf.join("");
};

this["JST"]["private_message"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),id = locals_.id,title = locals_.title;buf.push("<div" + (jade.attrs({ 'data-user-id':(id), "class": [('popup')] }, {"data-user-id":true})) + "><div class=\"title\"><div class=\"title_name\"><span class=\"icon icon-bubbles\"></span> " + (jade.escape((jade.interp = title) == null ? '' : jade.interp)) + "</div><div class=\"title_buttons\"><div class=\"title_button title_minimize\"><div class=\"icon icon-minus\"></div></div><div class=\"title_button title_close\"><div class=\"icon icon-close\"></div></div></div></div><div class=\"popup_body\"><div class=\"popup_menu clearfix\"><div class=\"popup_menu_button challenge_button\">Challenge</div></div><div class=\"challenge hidden\"></div><div class=\"popup_messages\"></div><div class=\"chat_input_pane\"><div class=\"chat_input_wrapper\"><input type=\"text\" placeholder=\"Send a message...\" class=\"chat_input\"/></div></div></div></div>");;return buf.join("");
};

this["JST"]["replay"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),replay = locals_.replay,window = locals_.window;buf.push("<div class=\"span4 p2 mb1 clickable-box relative\"><a" + (jade.attrs({ 'href':(replay.url()) }, {"href":true})) + "><strong class=\"block\">" + (jade.escape(null == (jade.interp = replay.get('name')) ? "" : jade.interp)) + "</strong></a>" + (jade.escape(null == (jade.interp = replay.getFormat()) ? "" : jade.interp)) + "<span class=\"block align-right tiny-type mt1 grey\"><span class=\"left\">" + (jade.escape(null == (jade.interp = replay.getCreatedAt()) ? "" : jade.interp)) + "</span><span" + (jade.attrs({ 'data-replay-id':(replay.id), 'data-cid':(replay.cid), "class": [('fake_link'),('delete-replay')] }, {"data-replay-id":true,"data-cid":true})) + "><span class=\"icon-remove\"></span>Delete</span></span><div class=\"show_spinner flex-center fill bg-faded-white hidden\">" + (null == (jade.interp = window.JST['spinner']()) ? "" : jade.interp) + "</div></div>");;return buf.join("");
};

this["JST"]["spinner"] = function anonymous(locals
/**/) {
var buf = [];
buf.push("<div class=\"spinner\"><div class=\"spinner-container container1\"><div class=\"circle1\"></div><div class=\"circle2\"></div><div class=\"circle3\"></div><div class=\"circle4\"></div></div><div class=\"spinner-container container2\"><div class=\"circle1\"></div><div class=\"circle2\"></div><div class=\"circle3\"></div><div class=\"circle4\"></div></div><div class=\"spinner-container container3\"><div class=\"circle1\"></div><div class=\"circle2\"></div><div class=\"circle3\"></div><div class=\"circle4\"></div></div></div>");;return buf.join("");
};

this["JST"]["team_dropdown"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),team = locals_.team,window = locals_.window,teams = locals_.teams;if ( team)
{
buf.push("<div>" + (jade.escape(null == (jade.interp = team.getName()) ? "" : jade.interp)) + "</div>" + (null == (jade.interp = window.JST['team_small']({team: team, window: window})) ? "" : jade.interp));
}
else
{
// iterate teams
;(function(){
  var $$obj = teams;
  if ('number' == typeof $$obj.length) {

    for (var i = 0, $$l = $$obj.length; i < $$l; i++) {
      var team = $$obj[i];

buf.push("<li><a" + (jade.attrs({ 'data-slot':(i), "class": [('select-team-dropdown-item')] }, {"data-slot":true})) + "><div>" + (jade.escape(null == (jade.interp = team.getName()) ? "" : jade.interp)) + "</div>" + (null == (jade.interp = window.JST['team_small']({team: team, window: window})) ? "" : jade.interp) + "</a></li>");
    }

  } else {
    var $$l = 0;
    for (var i in $$obj) {
      $$l++;      var team = $$obj[i];

buf.push("<li><a" + (jade.attrs({ 'data-slot':(i), "class": [('select-team-dropdown-item')] }, {"data-slot":true})) + "><div>" + (jade.escape(null == (jade.interp = team.getName()) ? "" : jade.interp)) + "</div>" + (null == (jade.interp = window.JST['team_small']({team: team, window: window})) ? "" : jade.interp) + "</a></li>");
    }

  }
}).call(this);

buf.push("<li><a class=\"build-team-option\">Build a Team</a></li>");
};return buf.join("");
};

this["JST"]["team_small"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),team = locals_.team,window = locals_.window;if ( team.hasPBV())
{
buf.push("<div class=\"team-pbv\">PBV: ");
if ( team.getPBV() <= team.getMaxPBV())
{
buf.push(jade.escape(null == (jade.interp = team.getPBV()) ? "" : jade.interp));
}
else
{
buf.push("<span class=\"red\">" + (jade.escape(null == (jade.interp = team.getPBV()) ? "" : jade.interp)) + "</span>");
}
buf.push("/" + (jade.escape((jade.interp = team.getMaxPBV()) == null ? '' : jade.interp)) + "</div>");
}
else if ( team.hasTier())
{
buf.push("<div class=\"team-tier\">Tier: ");
if ( team.getTier().tierRank <= team.getMaxTier().tierRank)
{
buf.push(jade.escape(null == (jade.interp = team.getTier().humanName) ? "" : jade.interp));
}
else
{
buf.push("<span class=\"red\">" + (jade.escape(null == (jade.interp = team.getTier().humanName) ? "" : jade.interp)) + "</span>");
}
buf.push("/" + (jade.escape((jade.interp = team.getMaxTier().humanName) == null ? '' : jade.interp)) + "</div>");
}
buf.push("<div class=\"team_icons clearfix\">");
// iterate team.get('pokemon').models
;(function(){
  var $$obj = team.get('pokemon').models;
  if ('number' == typeof $$obj.length) {

    for (var i = 0, $$l = $$obj.length; i < $$l; i++) {
      var pokemon = $$obj[i];

var style = window.PokemonIconBackground(pokemon)
buf.push("<div" + (jade.attrs({ 'data-index':(i), 'style':(style), "class": [('left'),('pokemon_icon')] }, {"data-index":true,"style":true})) + "></div>");
    }

  } else {
    var $$l = 0;
    for (var i in $$obj) {
      $$l++;      var pokemon = $$obj[i];

var style = window.PokemonIconBackground(pokemon)
buf.push("<div" + (jade.attrs({ 'data-index':(i), 'style':(style), "class": [('left'),('pokemon_icon')] }, {"data-index":true,"style":true})) + "></div>");
    }

  }
}).call(this);

buf.push("</div>");;return buf.join("");
};

this["JST"]["teambuilder/main"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),window = locals_.window;buf.push("<div class=\"teambuilder\"><div class=\"display_pokemon\"><div class=\"team_meta clearfix\"><div contenteditable=\"true\" class=\"team_name\"></div><div class=\"team_meta_buttons\"><div class=\"dropdown change-format-dropdown left\"><div data-toggle=\"dropdown\" class=\"current-format button dropdown-toggle\"></div>");
var allformats = window.PokeBattle.conditions.Formats()
buf.push("<ul role=\"menu\" class=\"dropdown-menu\">");
// iterate allformats
;(function(){
  var $$obj = allformats;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'href':("#"), 'data-format':("" + (format.name) + "") }, {"href":true,"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'href':("#"), 'data-format':("" + (format.name) + "") }, {"href":true,"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  }
}).call(this);

buf.push("</ul></div><div class=\"button button_blue save_team disabled\">Save</div><div class=\"button go_back\">Back</div></div></div><div class=\"navigation\"><ul class=\"pokemon_list clearfix\"></ul><div class=\"nav-button add_pokemon\">+ Add Pokemon</div></div><div class=\"pokemon_edit\"></div></div><div class=\"display_teams\"></div></div>");;return buf.join("");
};

this["JST"]["teambuilder/moves"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),pokemon = locals_.pokemon,window = locals_.window;buf.push("<div class=\"selected_moves\"><div class=\"header clearfix\"><div class=\"moves-label left\">Moves:</div><a" + (jade.attrs({ 'href':(pokemon.getPokedexUrl()), 'target':('_blank'), "class": [('dex-link'),('right')] }, {"href":true,"target":true})) + ">" + (jade.escape(null == (jade.interp = "View " + pokemon.get('species') + " Movesets") ? "" : jade.interp)) + "</a></div><div class=\"row-fluid\"><div class=\"span3\"><input" + (jade.attrs({ 'type':("text"), 'value':(pokemon.get("moves")[0]) }, {"type":true,"value":true})) + "/></div><div class=\"span3\"><input" + (jade.attrs({ 'type':("text"), 'value':(pokemon.get("moves")[1]) }, {"type":true,"value":true})) + "/></div><div class=\"span3\"><input" + (jade.attrs({ 'type':("text"), 'value':(pokemon.get("moves")[2]) }, {"type":true,"value":true})) + "/></div><div class=\"span3\"><input" + (jade.attrs({ 'type':("text"), 'value':(pokemon.get("moves")[3]) }, {"type":true,"value":true})) + "/></div></div></div><table class=\"table table-hover table-moves\"><thead><tr><th>Name</th><th>Type</th><th>Cat.</th><th>Power</th><th class=\"acc\">Acc.</th><th class=\"pp\">PP</th><th class=\"description\">Description</th></tr></thead><tbody>");
// iterate pokemon.getMovepool()
;(function(){
  var $$obj = pokemon.getMovepool();
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var move = $$obj[$index];

buf.push("<tr" + (jade.attrs({ 'data-move-id':(move.name), 'data-move-search-id':(move.name.replace(/\s+|-/g, "")) }, {"data-move-id":true,"data-move-search-id":true})) + "><td>" + (jade.escape(null == (jade.interp = move.name) ? "" : jade.interp)) + "</td><td><img" + (jade.attrs({ 'src':(window.TypeSprite(move.type)), 'alt':(move.type) }, {"src":true,"alt":true})) + "/></td><td>");
var damageFriendly = move.damage[0].toUpperCase() + move.damage.substr(1)
buf.push("<img" + (jade.attrs({ 'src':(window.CategorySprite(move.damage)), 'alt':(damageFriendly) }, {"src":true,"alt":true})) + "/></td><td>" + (jade.escape(null == (jade.interp = (move.power == 0) ? "-" : move.power) ? "" : jade.interp)) + "</td><td class=\"acc\">" + (jade.escape(null == (jade.interp = (move.accuracy == 0) ? "-" : move.accuracy + '%') ? "" : jade.interp)) + "</td><td class=\"pp\">" + (jade.escape(null == (jade.interp = move.pp) ? "" : jade.interp)) + "</td><td class=\"description\">" + (jade.escape(null == (jade.interp = move.description) ? "" : jade.interp)) + "</td></tr>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var move = $$obj[$index];

buf.push("<tr" + (jade.attrs({ 'data-move-id':(move.name), 'data-move-search-id':(move.name.replace(/\s+|-/g, "")) }, {"data-move-id":true,"data-move-search-id":true})) + "><td>" + (jade.escape(null == (jade.interp = move.name) ? "" : jade.interp)) + "</td><td><img" + (jade.attrs({ 'src':(window.TypeSprite(move.type)), 'alt':(move.type) }, {"src":true,"alt":true})) + "/></td><td>");
var damageFriendly = move.damage[0].toUpperCase() + move.damage.substr(1)
buf.push("<img" + (jade.attrs({ 'src':(window.CategorySprite(move.damage)), 'alt':(damageFriendly) }, {"src":true,"alt":true})) + "/></td><td>" + (jade.escape(null == (jade.interp = (move.power == 0) ? "-" : move.power) ? "" : jade.interp)) + "</td><td class=\"acc\">" + (jade.escape(null == (jade.interp = (move.accuracy == 0) ? "-" : move.accuracy + '%') ? "" : jade.interp)) + "</td><td class=\"pp\">" + (jade.escape(null == (jade.interp = move.pp) ? "" : jade.interp)) + "</td><td class=\"description\">" + (jade.escape(null == (jade.interp = move.description) ? "" : jade.interp)) + "</td></tr>");
    }

  }
}).call(this);

buf.push("</tbody></table>");;return buf.join("");
};

this["JST"]["teambuilder/pokemon"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),speciesList = locals_.speciesList,itemList = locals_.itemList,window = locals_.window;var printStat_mixin = function(statName, keyName){
var block = this.block, attributes = this.attributes || {}, escaped = this.escaped || {};
buf.push("<tr><td class=\"stat-label\"><strong>" + (jade.escape((jade.interp = statName) == null ? '' : jade.interp)) + ":</strong></td><td class=\"ev-range-cell\"><input" + (jade.attrs({ 'type':("range"), 'min':("0"), 'max':("252"), 'step':("4"), 'data-stat':(keyName), "class": [('ev-entry')] }, {"type":true,"min":true,"max":true,"step":true,"data-stat":true})) + "/></td><td class=\"ev-cell\"><input" + (jade.attrs({ 'type':("text"), 'data-stat':(keyName), "class": [('ev-entry')] }, {"type":true,"data-stat":true})) + "/></td><td class=\"iv-cell\"><input" + (jade.attrs({ 'type':("text"), 'data-stat':(keyName), "class": [('iv-entry')] }, {"type":true,"data-stat":true})) + "/></td><td" + (jade.attrs({ 'data-stat':(keyName), "class": [('base-stat')] }, {"data-stat":true})) + "></td><td" + (jade.attrs({ 'data-stat':(keyName), "class": [('stat-total')] }, {"data-stat":true})) + "></td></tr>");
};
buf.push("<div class=\"meta-info clearfix\"><div class=\"left-side\"><div class=\"species\"><label class=\"filter-tier\"><input type=\"checkbox\" class=\"filter-tier-box\"/>Filter to Current Tier</label><select class=\"sortSpecies\"><option>" + (jade.escape(null == (jade.interp = "Sort") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Sort by Dexnumber") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Invert by Dexnumber") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Sort Alphabetically") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Invert Alphabetically") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Sort by Tier") ? "" : jade.interp)) + "</option><option>" + (jade.escape(null == (jade.interp = "Invert by Tier") ? "" : jade.interp)) + "</option></select><select class=\"species_list\"><option></option>");
// iterate speciesList
;(function(){
  var $$obj = speciesList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var species = $$obj[$index];

buf.push("<option" + (jade.attrs({ 'value':(species) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = species) ? "" : jade.interp)) + "</option>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var species = $$obj[$index];

buf.push("<option" + (jade.attrs({ 'value':(species) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = species) ? "" : jade.interp)) + "</option>");
    }

  }
}).call(this);

buf.push("</select><div class=\"species-info\"></div></div><div class=\"non-stats\"><div class=\"teambuilder_row format_row\"><div class=\"teambuilder_col non-stat label formatname\"></div><div class=\"teambuilder_col\"><span class=\"individual-format\"></span><div class=\"right\"><span class=\"total-format\"></span>/<span class=\"max-format\"></span></div></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Nickname:</div><div class=\"teambuilder_col\"><input type=\"text\" class=\"selected_nickname\"/></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Ability:</div><div class=\"teambuilder_col\"><select class=\"selected_ability\"></select></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Nature:</div><div class=\"teambuilder_col\"><select class=\"selected_nature\"></select></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Item:</div><div class=\"teambuilder_col\"><select class=\"selected_item\"><option value=\"\"></option>");
// iterate itemList
;(function(){
  var $$obj = itemList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var itemName = $$obj[$index];

buf.push("<option" + (jade.attrs({ 'value':(itemName) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = itemName) ? "" : jade.interp)) + "</option>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var itemName = $$obj[$index];

buf.push("<option" + (jade.attrs({ 'value':(itemName) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = itemName) ? "" : jade.interp)) + "</option>");
    }

  }
}).call(this);

buf.push("</select></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Gender:</div><div class=\"teambuilder_col\"><select class=\"selected_gender\"></select></div></div><div class=\"teambuilder_row\"><div class=\"teambuilder_col non-stat-label\">Level:</div><div class=\"teambuilder_col\"><input type=\"text\" class=\"selected_level\"/></div></div></div></div><div class=\"right-side\"><table class=\"stats\"><thead><th></th><th class=\"ev-range-cell\"></th><th class=\"ev-cell\">EV</th><th class=\"iv-cell\">IV</th><th>Base</th><th>Stat</th></thead><tbody>");
printStat_mixin("HP", "hp");
printStat_mixin("Attack", "attack");
printStat_mixin("Defense", "defense");
printStat_mixin("Sp. Attack", "specialAttack");
printStat_mixin("Sp. Defense", "specialDefense");
printStat_mixin("Speed", "speed");
buf.push("<tr><td></td><td colspan=\"5\"><div class=\"remaining-evs\">Remaining EVs: <span class=\"remaining-evs-amount\"></span></div><div class=\"hidden-power\">Hidden Power: <select class=\"select-hidden-power\">");
// iterate window.HiddenPower.BW.ivs
;(function(){
  var $$obj = window.HiddenPower.BW.ivs;
  if ('number' == typeof $$obj.length) {

    for (var type = 0, $$l = $$obj.length; type < $$l; type++) {
      var value = $$obj[type];

var displayType = type[0].toUpperCase() + type.substr(1)
buf.push("<option" + (jade.attrs({ 'value':(type) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = displayType) ? "" : jade.interp)) + "</option>");
    }

  } else {
    var $$l = 0;
    for (var type in $$obj) {
      $$l++;      var value = $$obj[type];

var displayType = type[0].toUpperCase() + type.substr(1)
buf.push("<option" + (jade.attrs({ 'value':(type) }, {"value":true})) + ">" + (jade.escape(null == (jade.interp = displayType) ? "" : jade.interp)) + "</option>");
    }

  }
}).call(this);

buf.push("</select></div></td></tr></tbody></table></div><div class=\"moves-section clearfix\"></div></div>");;return buf.join("");
};

this["JST"]["teambuilder/pokemon_list"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),pokemonList = locals_.pokemonList,window = locals_.window;// iterate pokemonList
;(function(){
  var $$obj = pokemonList;
  if ('number' == typeof $$obj.length) {

    for (var i = 0, $$l = $$obj.length; i < $$l; i++) {
      var pokemon = $$obj[i];

buf.push("<li" + (jade.attrs({ 'data-pokemon-index':(i), 'data-pokemon-cid':(pokemon.cid) }, {"data-pokemon-index":true,"data-pokemon-cid":true})) + ">");
var style = window.PokemonIconBackground(pokemon)
buf.push("<div" + (jade.attrs({ 'style':(style), "class": [('pokemon_icon')] }, {"style":true})) + "></div><div class=\"pokemon-middle\">");
if ( pokemon.isNull)
{
buf.push("<em class=\"name\">Empty</em>");
}
else
{
buf.push("<div class=\"name\">" + (jade.escape(null == (jade.interp = pokemon.get("species")) ? "" : jade.interp)) + "</div>");
if ( pokemon.getTeam().hasPBV())
{
buf.push("<div class=\"pokemon-pbv\">PBV: <span class=\"pbv-value\">" + (jade.escape(null == (jade.interp = pokemon.getPBV()) ? "" : jade.interp)) + "</span></div>");
}
else if ( pokemon.getTeam().hasTier())
{
buf.push("<div class=\"pokemon-pbv\">Tier: <span class=\"pbv-value\">" + (jade.escape(null == (jade.interp = pokemon.getTier().humanName) ? "" : jade.interp)) + "</span></div>");
}
}
buf.push("</div></li>");
    }

  } else {
    var $$l = 0;
    for (var i in $$obj) {
      $$l++;      var pokemon = $$obj[i];

buf.push("<li" + (jade.attrs({ 'data-pokemon-index':(i), 'data-pokemon-cid':(pokemon.cid) }, {"data-pokemon-index":true,"data-pokemon-cid":true})) + ">");
var style = window.PokemonIconBackground(pokemon)
buf.push("<div" + (jade.attrs({ 'style':(style), "class": [('pokemon_icon')] }, {"style":true})) + "></div><div class=\"pokemon-middle\">");
if ( pokemon.isNull)
{
buf.push("<em class=\"name\">Empty</em>");
}
else
{
buf.push("<div class=\"name\">" + (jade.escape(null == (jade.interp = pokemon.get("species")) ? "" : jade.interp)) + "</div>");
if ( pokemon.getTeam().hasPBV())
{
buf.push("<div class=\"pokemon-pbv\">PBV: <span class=\"pbv-value\">" + (jade.escape(null == (jade.interp = pokemon.getPBV()) ? "" : jade.interp)) + "</span></div>");
}
else if ( pokemon.getTeam().hasTier())
{
buf.push("<div class=\"pokemon-pbv\">Tier: <span class=\"pbv-value\">" + (jade.escape(null == (jade.interp = pokemon.getTier().humanName) ? "" : jade.interp)) + "</span></div>");
}
}
buf.push("</div></li>");
    }

  }
}).call(this);
;return buf.join("");
};

this["JST"]["teambuilder/species"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),window = locals_.window,pokemon = locals_.pokemon;buf.push("<div class=\"forme-sprite-box\">");
var url = window.PokemonSprite(pokemon)
buf.push("<img" + (jade.attrs({ 'src':(url), 'alt':(""), "class": [('forme-sprite')] }, {"src":true,"alt":true})) + "/></div><div class=\"species-types\"><div" + (jade.attrs({ "class": [('shiny-switch'),('selected_shininess'),((pokemon.get("shiny") ? "selected" : ""))] }, {"class":true})) + "><div class=\"top\"></div><div class=\"bottom\"></div></div>");
// iterate pokemon.getForme().types
;(function(){
  var $$obj = pokemon.getForme().types;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var type = $$obj[$index];

buf.push("<img" + (jade.attrs({ 'src':(window.TypeSprite(type)), 'alt':(type) }, {"src":true,"alt":true})) + "/>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var type = $$obj[$index];

buf.push("<img" + (jade.attrs({ 'src':(window.TypeSprite(type)), 'alt':(type) }, {"src":true,"alt":true})) + "/>");
    }

  }
}).call(this);

buf.push("<div class=\"happiness-switch selected_happiness\"></div></div>");
var formes = pokemon.getSelectableFormes()
if ( formes.length > 1)
{
buf.push("<div class=\"teambuilder_row\"><div class=\"teambuilder_col\">Forme:</div><select class=\"teambuilder_col selected-forme\">");
// iterate formes
;(function(){
  var $$obj = formes;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var forme = $$obj[$index];

var displayedForme = forme[0].toUpperCase() + forme.substr(1)
var selected = (forme === pokemon.get('forme'))
buf.push("<option" + (jade.attrs({ 'value':(forme), 'selected':(selected) }, {"value":true,"selected":true})) + ">" + (jade.escape(null == (jade.interp = displayedForme) ? "" : jade.interp)) + "</option>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var forme = $$obj[$index];

var displayedForme = forme[0].toUpperCase() + forme.substr(1)
var selected = (forme === pokemon.get('forme'))
buf.push("<option" + (jade.attrs({ 'value':(forme), 'selected':(selected) }, {"value":true,"selected":true})) + ">" + (jade.escape(null == (jade.interp = displayedForme) ? "" : jade.interp)) + "</option>");
    }

  }
}).call(this);

buf.push("</select></div>");
};return buf.join("");
};

this["JST"]["teambuilder/team"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),team = locals_.team,window = locals_.window;buf.push("<div" + (jade.attrs({ 'data-id':(team.id), 'data-cid':(team.cid), "class": [('left'),('select-team'),('clickable-box'),('p1'),('mt1'),('ml1'),('relative')] }, {"data-id":true,"data-cid":true})) + "><h2 class=\"go-to-team fake_link\">" + (jade.escape(null == (jade.interp = team.getName()) ? "" : jade.interp)) + "</h2><div class=\"arrow-up\"></div><div class=\"arrow-down\"></div>" + (null == (jade.interp = window.JST['team_small']({team: team, window: window})) ? "" : jade.interp) + "<div class=\"team-meta\"><span class=\"fake_link export-team\"><span class=\"icon-upload\"></span> Export</span> | <span class=\"fake_link clone-team\"><span class=\"icon-copy\"></span> Clone</span> | <span class=\"fake_link delete-team\"><span class=\"icon-remove\"></span> Delete</span></div><div class=\"show_spinner flex-center fill bg-faded-white rounded hidden\">" + (null == (jade.interp = window.JST['spinner']()) ? "" : jade.interp) + "</div></div>");;return buf.join("");
};

this["JST"]["teambuilder/teams"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),teams = locals_.teams,window = locals_.window;buf.push("<h2>Your teams</h2><div class=\"teambuilder_teams clearfix\">");
// iterate teams
;(function(){
  var $$obj = teams;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var team = $$obj[$index];

if ( team.isDefault)
{
buf.push(null == (jade.interp = window.JST['teambuilder/team']({team: team, window: window})) ? "" : jade.interp);
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var team = $$obj[$index];

if ( team.isDefault)
{
buf.push(null == (jade.interp = window.JST['teambuilder/team']({team: team, window: window})) ? "" : jade.interp);
}
    }

  }
}).call(this);

// iterate teams
;(function(){
  var $$obj = teams;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var team = $$obj[$index];

buf.push(null == (jade.interp = window.JST['teambuilder/team']({team: team, window: window})) ? "" : jade.interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var team = $$obj[$index];

buf.push(null == (jade.interp = window.JST['teambuilder/team']({team: team, window: window})) ? "" : jade.interp);
    }

  }
}).call(this);

buf.push("</div><div class=\"button button_blue add-new-team\">Add new team</div><div class=\"button import-team\">Import</div>");
if ( window.PokeBattle.username == "Deukhoofd" || window.PokeBattle.username == "thesuzerain")
{
buf.push("<div class=\"Team-Admin\"><h5>Admin Commands</h5><div class=\"button get-teams\">Get all teams</div><br/>Get Random teams");
var allformats = window.PokeBattle.conditions.Formats()
buf.push("<div class=\"dropdown random-team-admin\"><div data-toggle=\"dropdown\" class=\"current-format-random-admin button dropdown-toggle\"><a" + (jade.attrs({ 'id':('currentselectedrandomadminformat'), 'href':("#"), 'data-format':("" + (allformats[window.DEFAULT_FORMAT].name) + "") }, {"href":true,"data-format":true})) + ">" + (jade.escape((jade.interp = allformats[window.DEFAULT_FORMAT].humanName) == null ? '' : jade.interp)) + "</a></div><ul role=\"menu\" class=\"dropdown-menu\">");
// iterate allformats
;(function(){
  var $$obj = allformats;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'href':("#"), 'data-format':("" + (format.name) + "") }, {"href":true,"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var format = $$obj[$index];

buf.push("<li><a" + (jade.attrs({ 'href':("#"), 'data-format':("" + (format.name) + "") }, {"href":true,"data-format":true})) + ">" + (jade.escape((jade.interp = format.humanName) == null ? '' : jade.interp)) + "</a></li>");
    }

  }
}).call(this);

buf.push("</ul><div class=\"button getrandomteams\">1</div><div class=\"button getrandomteams\">5</div><div class=\"button getrandomteams\">10</div><div class=\"button getrandomteams\">25</div></div></div>");
};return buf.join("");
};

this["JST"]["user_list"] = function anonymous(locals
/**/) {
var buf = [];
var locals_ = (locals || {}),userList = locals_.userList;// iterate userList
;(function(){
  var $$obj = userList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

if ( user.isAlt())
{
buf.push("<li>" + (jade.escape(null == (jade.interp = user.getDisplayName()) ? "" : jade.interp)) + "</li>");
}
else
{
buf.push("<li" + (jade.attrs({ 'data-user-id':(user.id), "class": [('fake_link'),('open_pm')] }, {"data-user-id":true})) + ">" + (jade.escape(null == (jade.interp = user.getDisplayName()) ? "" : jade.interp)) + "</li>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

if ( user.isAlt())
{
buf.push("<li>" + (jade.escape(null == (jade.interp = user.getDisplayName()) ? "" : jade.interp)) + "</li>");
}
else
{
buf.push("<li" + (jade.attrs({ 'data-user-id':(user.id), "class": [('fake_link'),('open_pm')] }, {"data-user-id":true})) + ">" + (jade.escape(null == (jade.interp = user.getDisplayName()) ? "" : jade.interp)) + "</li>");
}
    }

  }
}).call(this);
;return buf.join("");
};