class @TeambuilderView extends Backbone.View
  template: JST['teambuilder/main']
  teamTemplate: JST['teambuilder/team']
  teamsTemplate: JST['teambuilder/teams']
  pokemonListTemplate: JST['teambuilder/pokemon_list']

  events:
    # Team view
    'click .add-new-team':  'addNewTeamEvent'
    'click .export-team' :  'exportTeam'
    'click .clone-team'  :  'cloneTeam'
    'click .delete-team' :  'deleteTeamEvent'
    'click .go-to-team'  :  'clickTeam'
    'click .import-team' :  'renderImportTeamModal'
    'click .get-teams'   :  'getAllTeamsAdmin'
    'click .arrow-up'    :  'moveTeamUp'
    'click .arrow-down'  :  'moveTeamDown'

    # Teambuild view
    'click .change-format-dropdown a': 'changeTeamFormat'
    'blur .team_name': 'blurTeamName'
    'keypress .team_name': 'keypressTeamName'
    'click .go_back': 'goBackToOverview'
    'click .pokemon_list li': 'clickPokemon'
    'click .add_pokemon': 'addNewPokemonEvent'
    'click .save_team': 'saveTeam'

    'click .random-team-admin a': 'randomTeamTierChange'
    'click .getrandomteams': 'getRandomTeams'

  initialize: (attributes) =>
    @selectedPokemon = 0
    @selectedTeam = null
    @randomformatadmin = window.DEFAULT_FORMAT

    @render()

    @listenTo(PokeBattle.TeamStore, 'reset', @resetTeams)
    @listenTo(PokeBattle.TeamStore, 'add', @addNewTeam)
    @listenTo(PokeBattle.TeamStore, 'remove', @deleteTeam)
    @listenTo(PokeBattle.TeamStore, 'change:id', @changeTeamId)
    @listenTo(PokeBattle.TeamStore, 'reset', @renderTeams)
    @listenTo(PokeBattle.TeamStore, 'saving', @renderSaving)
    @listenTo(PokeBattle.TeamStore, 'saved', @renderSaved)
    @listenTo PokeBattle.TeamStore, 'render', (team) =>
      @renderTeams()
      if @getSelectedTeam() && team.id == @getSelectedTeam().id
        @setSelectedTeam(team)

    @pokemonEditView = new PokemonEditView(
      el: @$('.pokemon_edit')
      onPokemonChange: (newPokemon) =>
        team = @getSelectedTeam()
        team.replace(@selectedPokemon, newPokemon)
        @renderTier()
        
    )

  clickTeam: (e) =>
    $team = $(e.currentTarget).closest('.select-team')
    team = PokeBattle.TeamStore.get($team.data('cid'))
    @setSelectedTeam(team)

  clickPokemon: (e) =>
    $listItem = $(e.currentTarget)
    index = @$('.pokemon_list li').index($listItem)
    @setSelectedPokemonIndex(index)

  attachEventsToTeam: (team) =>
    return  if team.attachedTeambuildEvents

    @listenTo(team, 'add:pokemon', @renderPokemon)

    # Todo: Make this perform better
    @listenTo(team, 'change:pokemon[*].species change:pokemon[*].forme', (pokemon) =>
      @renderPokemonList()
      @renderPokemon(pokemon)
    )

    @listenTo(team, 'add:pokemon remove:pokemon', @renderPokemonList)
    @listenTo(team, 'reset:pokemon', (=> @changeTeam(team)))
    @listenTo(team, 'change nested-change reset:pokemon add:pokemon remove:pokemon', @dirty)
    @listenTo(team, 'change:pokemon[*] reset:pokemon add:pokemon remove:pokemon', @renderTier, @renderPBV)

    # A temporary flag to attach until the teambuilder view is refactored
    team.attachedTeambuildEvents = true

  addEmptyPokemon: (team) =>
    team.get('pokemon').add(new NullPokemon())

  addNewTeamEvent: (e) =>
    team = new Team()
    PokeBattle.TeamStore.add(team)
    team.save()

  getAllTeamsAdmin: (e) =>
    console.log('this')
    PokeBattle.primus.send('requestTeams', true)

  addNewTeam: (team) =>
    @addEmptyPokemon(team)  while team.get('pokemon').length < 6
    @$('.teambuilder_teams').append @teamTemplate({team, window})
    @attachEventsToTeam(team)

  resetTeams: (teamStore) =>
    teamStore.forEach (team) =>
      @attachEventsToTeam(team)

  cloneTeam: (e) =>
    $team = $(e.currentTarget).closest('.select-team')
    cid = $team.data('cid')
    clone = @getTeam(cid).clone().set("id", null)
    PokeBattle.TeamStore.add(clone)
    clone.save()
    return false

  deleteTeamEvent: (e) =>
    return false  if !confirm("Do you really want to delete this team?")
    $team = $(e.currentTarget).closest('.select-team')
    team = @getTeam($team.data('cid'))
    PokeBattle.TeamStore.remove(team)
    team.destroy()
    return false

  deleteTeam: (team) =>
    @$(".select-team[data-cid=#{team.cid}]").remove()

  changeTeam: (team) =>
    html = $(@teamTemplate({team, window})).html()
    @$(".select-team[data-cid=#{team.cid}]").html(html)

  changeTeamId: (team) =>
    @$(".select-team[data-cid=#{team.cid}]").attr('data-id', team.id)

  exportTeam: (e) =>
    $team = $(e.currentTarget).closest('.select-team')
    id = $team.data('id')
    name = $team.data('name')
    if not @getTeam(id).hasNonNullPokemon()
      alert("You cannot export empty teams. Please add some pokemon first.")
      return false

    teamJSON = @getTeam(id).toNonNullJSON()
    teamString = PokeBattle.exportTeam(teamJSON.pokemon)

    $modal = PokeBattle.modal 'modals/export_team', ($modal) =>
      $modal.find('.exported-team').val(teamString)
      $modal.on 'click', '.import-team-submit', (e) =>
        teamString = $modal.find('.exported-team').val()
        pokemonJSON = PokeBattle.parseTeam(teamString)
        errors = @validateImportedTeam(pokemonJSON)
        if errors.length > 0
          listErrors = errors.map((e) -> "<li>#{e}</li>").join('')
          $errors = $modal.find('.form-errors')
          $errors.html("<ul>#{listErrors}</ul>").removeClass('hidden')
        else
          jint = 0
          for jPkmn in pokemonJSON
            @getTeam(id).replace(jint, jPkmn)
            jint = jint+1
          @getTeam(id).save()
          $modal.modal('hide')
        return false
    $modal.find('.exported-team').first().focus()


    return false

  addNewPokemonEvent: =>
    @addNewPokemon(@getSelectedTeam())

  addNewPokemon: (team) =>
    @addEmptyPokemon(team)
    @$('.pokemon_list li').last().click()

  saveTeam: =>
    clone = @getSelectedTeam()
    team = PokeBattle.TeamStore.get(clone.id)
    team.save(clone.toJSON(), silent: true)
    @resetHeaderButtons()

  changeTeamFormat: (e) =>
    $link = $(e.currentTarget)
    format = $link.data('format')
    team = @getSelectedTeam()
    realformat = window.PokeBattle.conditions.Formats_[format]
    if format != team.get('generation')
      team.set('generation', format)
      @renderTeam()
      @dirty() # renderTeam() removes dirty, so call it again

  randomTeamTierChange: (e) =>
    @$("#currentselectedrandomadminformat")[0].innerHTML = e.currentTarget.text
    @randomformatadmin = e.currentTarget.dataset.format
  getRandomTeams: (e) =>
    number = parseInt(e.currentTarget.textContent, 10)
    PokeBattle.primus.send('getRandomTeamsAdmin', @randomformatadmin, number)

  setSelectedPokemonIndex: (index) =>
    pokemon = @getSelectedTeam().at(index)
    @selectedPokemon = index

    # Render the pokemon
    @pokemonEditView.setPokemon(pokemon)
    @renderPokemon(pokemon)

    # Set the correct list item to active
    @$(".navigation li").removeClass("active")
    @$(".navigation li").eq(index).addClass("active")

  getSelectedPokemon: =>
    @getSelectedTeam().at(@selectedPokemon)

  setSelectedTeam: (team) =>
    # Duplicate the team, so that changes don't stick until saved
    @selectedTeam = team.clone()
    @selectedTeam.id = team.id
    @selectedTeam.cid = team.cid
    @selectedPokemon = 0
    @attachEventsToTeam(@selectedTeam)
    @renderTeam()

  getAllTeams: =>
    PokeBattle.TeamStore.models

  getSelectedTeam: =>
    @selectedTeam

  getTeam: (idx) =>
    PokeBattle.TeamStore.get(idx)

  blurTeamName: =>
    teamName = @$('.team_name').text()
    @getSelectedTeam().set('name', teamName)

  keypressTeamName: (e) =>
    if e.which == 13  # [Enter]
      @$('.team_name').blur()

  goBackToOverview: =>
    @renderTeams()

  dirty: =>
    @$('.go_back').text('Discard changes')
    @$('.save_team').removeClass('disabled')

  resetHeaderButtons: =>
    @$('.go_back').text('Back')
    @$('.save_team').addClass('disabled')

  render: =>
    @$el.html @template(pokemon: @getSelectedTeam(), selected: @selectedPokemon, window: window)
    @renderTeams()

  renderTeams: =>
    allteams = sortByKey(@getAllTeams(), "id")
    @$('.display_teams').html @teamsTemplate(teams: allteams, window: window)
    @$('.display_teams').removeClass('hidden')
    @$('.display_pokemon').addClass('hidden')
    this

  renderTeam: =>
    team = @getSelectedTeam()
    @pokemonEditView.setFormat(team.get('generation')  || DEFAULT_FORMAT)
    @resetHeaderButtons()
    @renderFormat()
    @renderPokemonList()
    @setSelectedPokemonIndex(@selectedPokemon)
    @$('.team_name').text(team.getName())
    @$('.display_teams').addClass('hidden')
    @$('.display_pokemon').removeClass('hidden')

  renderPokemonList: =>
    team = @getSelectedTeam()
    $pokemon_list = @$(".pokemon_list").empty()
    $pokemon_list.html @pokemonListTemplate(window: window, pokemonList: team.get('pokemon').models)
    $pokemon_list.find("li[data-pokemon-index=#{@selectedPokemon}]").addClass("active")

    # NOTE: this isn't be used, and just amounts to hiding the button, however
    # we may re-enable this functionality in the future
    # Hide add pokemon if there's 6 pokemon
    if team.length < 6
      @$(".add_pokemon").show()
    else
      @$(".add_pokemon").hide()

  renderPokemon: (pokemon) =>
    @pokemonEditView.setPokemon(pokemon)

  renderPBV: (pokemon) =>
    if pokemon
      individualPBV = pokemon.getPBV()
      $listItem = @$(".pokemon_list li[data-pokemon-cid=#{pokemon.cid}]")
      $listItem.find(".pbv-value").text(individualPBV)

    totalPBV = @getSelectedTeam().getPBV()
    @pokemonEditView.setTeamPBV(totalPBV)
    @pokemonEditView.renderPBV()

  renderTier: (pokemon) =>
    if pokemon
      individualTier = pokemon.getTier()
    totalTier = @getSelectedTeam().getTier()
    @pokemonEditView.setTeamTier(totalTier)
    @pokemonEditView.renderTier()

  renderFormat: =>
    allformats = window.PokeBattle.conditions.Formats()
    format = @getSelectedTeam().get("generation")
    format = DEFAULT_FORMAT  if format not of allformats
    text = @$(".change-format-dropdown a[data-format='#{format}']").text()
    @$(".current-format").text(text)

  renderImportTeamModal: =>
    $modal = PokeBattle.modal 'modals/import_team', ($modal) =>
      $modal.on 'click', '.import-team-submit', (e) =>
        teamString = $modal.find('.imported-team').val()
        pokemonJSON = PokeBattle.parseTeam(teamString)
        errors = @validateImportedTeam(pokemonJSON)
        if errors.length > 0
          listErrors = errors.map((e) -> "<li>#{e}</li>").join('')
          $errors = $modal.find('.form-errors')
          $errors.html("<ul>#{listErrors}</ul>").removeClass('hidden')
        else
          team = new Team(pokemon: pokemonJSON, teambuilder: true)
          PokeBattle.TeamStore.add(team)
          team.save()
          $modal.find('.imported-team').val("")
          $modal.modal('hide')
        return false
    $modal.find('.imported-team').first().focus()

  validateImportedTeam: (json) =>
    errors = []
    pokemonSpecies = (pokemon.species  for pokemon in json)
    {SpeciesData} = window.Generations[DEFAULT_GENERATION.toUpperCase()]
    pokemonSpecies = pokemonSpecies.filter((s) -> s not of SpeciesData)
    if pokemonSpecies.length > 0
      errors.push(pokemonSpecies.map((n) -> "#{n} is not a valid Pokemon.")...)
      return errors
    return errors

  renderSaving: (team) =>
    $team = $(".select-team[data-cid='#{team.cid}']")
    $team.find('.show_spinner').removeClass('hidden')

  renderSaved: (team) =>
    $team = $(".select-team[data-cid='#{team.cid}']")
    $team.find('.show_spinner').addClass('hidden')

  moveTeamUp: (e) =>
    @resetTeams(PokeBattle.TeamStore)
    $team = $(e.currentTarget).closest('.select-team')
    id = $team.data('id')
    for indexno, team_ of @getAllTeams()
      if team_.id is id
        team = team_
    allTeams = sortByKey(@getAllTeams(), "id")
    index = -1
    for thisteam, num in allTeams
      index = num if thisteam.id is id
    if index is 0
      return
    upTeam = allTeams[index - 1]
    upID = upTeam.id
    downID = team.id
    upTeam.id = downID
    team.id = upID
    team.save()
    upTeam.save()
    @resetTeams(PokeBattle.TeamStore)
    @renderTeams()

  moveTeamDown: (e) =>
    @resetTeams(PokeBattle.TeamStore)
    $team = $(e.currentTarget).closest('.select-team')
    id = $team.data('id')
    for indexno, team_ of @getAllTeams()
      if team_.id is id
        team = team_
    allTeams = sortByKey(@getAllTeams(), "id")
    index = -1
    for thisteam, num in allTeams
      index = num if thisteam.id is id
    if (index is (allTeams.length - 1))
      return
    upTeam = allTeams[index + 1]
    upID = upTeam.id
    downID = team.id
    upTeam.id = downID
    team.id = upID
    team.save()
    upTeam.save()
    @resetTeams(PokeBattle.TeamStore)
    @renderTeams()

  sortByKey = (array, key) ->
    array.sort (a,b) ->
      if a[key] < b[key]
        -1
      else if a[key] > b[key]
        1
      else
        0